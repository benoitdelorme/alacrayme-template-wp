var App = function() {
  var cache = {},
      self = this;

  var init = function() {
    console.log("##APP START##");

    var loader = new Loader();
    initCache();
    open();
    
    var home = new Home();
    // var router = new Router();

    return self;
  };

  var initCache = function() {
    cache.root = document.querySelector(".root-content");
  };

  var initEvent = function() {

  };

  var initLoader = function() {

  };

  var resize = function() {

  };

  var open = function() {

  };

  var close = function() {

  };

  init();

}();

var Home = function() {
  var video1 = document.querySelectorAll('video')[0]
    , video2 = document.querySelectorAll('video')[1]
    , container = document.querySelectorAll('.block')[0];

  var init = function() {
    console.log("##LOAD HOME##")
    initEvents();
    initVideo();
  };

  var initEvents = function() {
    
  };

  var initVideo = function() {
    BackgroundSizeCover(video1, container);
    BackgroundSizeCover(video2, container);
  }

  init();
};

var BackgroundSizeCover = function(element, container) {

  var init = function() {
    initEvents();
  };

  var initEvents = function() {
    video.addEventListener('loadedmetadata', setVideoDimensions, false);
    window.addEventListener('resize', setVideoDimensions, false);
  }

  var setVideoDimensions = function () {
    // Video's intrinsic dimensions
    var w = element.videoWidth
      , h = element.videoHeight;

    // Intrinsic Ratio
    // Will be more than 1 if W > H and less if W < H
    var videoRatio = (w / h).toFixed(2);

    // Get the container's computed styles
    //
    // Also calculate the min dimensions required (this will be
    // the container dimentions)
    var containerStyles = window.getComputedStyle(container)
      , minW = parseInt( containerStyles.getPropertyValue('width') )
      , minH = parseInt( containerStyles.getPropertyValue('height') );

    // What's the min:intrinsic dimensions
    //
    // The idea is to get which of the container dimension
    // has a higher value when compared with the equivalents
    // of the video. Imagine a 1200x700 container and
    // 1000x500 video. Then in order to find the right balance
    // and do minimum scaling, we have to find the dimension
    // with higher ratio.
    //
    // Ex: 1200/1000 = 1.2 and 700/500 = 1.4 - So it is best to
    // scale 500 to 700 and then calculate what should be the
    // right width. If we scale 1000 to 1200 then the height
    // will become 600 proportionately.
    var widthRatio = minW / w
      , heightRatio = minH / h;

    // Whichever ratio is more, the scaling
    // has to be done over that dimension
    if (widthRatio > heightRatio) {
      var newWidth = minW;
      var newHeight = Math.ceil( newWidth / videoRatio );
    }
    else {
      var newHeight = minH;
      var newWidth = Math.ceil( newHeight * videoRatio );
    }

    element.style.width = newWidth + 'px';
    element.style.height = newHeight + 'px';
  };

  init();
};

var Loader = function() {
  var imgLoad = new imagesLoaded( '.root-content', { background: true }, function() {}),
  imgs = imgLoad.images,
  imgsLength = imgs.length,
  imgLoaded = 0,
  loader = document.querySelector(".oxo-loader"),
  numberNode = loader.querySelector(".number");

  var init = function() {
    preload();
  };

  var preload = function() {
    load();
  };

  var load = function() {
    console.log("##LOAD...##");

    if(imgsLength != 0) {
      function onAlways( instance ) {
        console.log('##All images are loaded !');
        onLoaded();
      }

      imgLoad.on( 'always', onAlways );

      imgLoad.on( 'progress', function( instance, image ) {
        imgLoaded++;
        console.log((100*(imgLoaded)/imgsLength));
        numberNode.innerHTML = Math.trunc(100*(imgLoaded)/imgsLength);
      });

    }else {
      console.log(100+"%");
      onLoaded();
    }
  };

  var onLoaded = function() {
    TweenMax.to(loader, 1.3, {y: 100+"%", ease: Expo.easeInOut, force3D: true, delay: 1});
  };

  init();
};
