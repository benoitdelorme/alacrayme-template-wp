var Loader = function() {
  var imgLoad = new imagesLoaded( '.root-content', { background: true }, function() {}),
  imgs = imgLoad.images,
  imgsLength = imgs.length,
  imgLoaded = 0,
  loader = document.querySelector(".oxo-loader"),
  numberNode = loader.querySelector(".number");

  var init = function() {
    preload();
  };

  var preload = function() {
    load();
  };

  var load = function() {
    console.log("##LOAD...##");

    if(imgsLength != 0) {
      function onAlways( instance ) {
        console.log('##All images are loaded !');
        onLoaded();
      }

      imgLoad.on( 'always', onAlways );

      imgLoad.on( 'progress', function( instance, image ) {
        imgLoaded++;
        console.log((100*(imgLoaded)/imgsLength));
        numberNode.innerHTML = Math.trunc(100*(imgLoaded)/imgsLength);
      });

    }else {
      console.log(100+"%");
      onLoaded();
    }
  };

  var onLoaded = function() {
    TweenMax.to(loader, 1.3, {y: 100+"%", ease: Expo.easeInOut, force3D: true, delay: 1});
  };

  init();
};
