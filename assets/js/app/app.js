var App = function() {
  var cache = {},
      self = this;

  var init = function() {
    console.log("##APP START##");

    var loader = new Loader();
    initCache();
    open();
    
    var home = new Home();
    // var router = new Router();

    return self;
  };

  var initCache = function() {
    cache.root = document.querySelector(".root-content");
  };

  var initEvent = function() {

  };

  var initLoader = function() {

  };

  var resize = function() {

  };

  var open = function() {

  };

  var close = function() {

  };

  init();

}();
