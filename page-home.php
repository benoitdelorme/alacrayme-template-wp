<?php
/**
 * The template for displaying pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package OxO
 * @subpackage OxO Template
 * @since OxO Template v1
 */

get_header();
$urlTemp = get_stylesheet_directory_uri();
?>

<div class="oxo-view home">
  <!-- test file -->
  <!-- <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/a/ae/Michel_Vuillermoz_20070511_Fnac_6.jpg/400px-Michel_Vuillermoz_20070511_Fnac_6.jpg" alt="" /> -->
  <div class="brands">

    <div class="block">
      <div class="button">
        <div class="line"></div>
      </div>
      <div class="layer"></div>
      <video src="<?php echo $urlTemp ?>/assets/video/SampleVideo_1280x720_1mb.mp4" autoplay loop muted playsinline></video>
    </div>

    <div class="block">
      <video src="<?php echo $urlTemp ?>/assets/video/SampleVideo_1280x720_1mb.mp4" autoplay loop muted playsinline></video>
    </div>
    
    <div class="layer"></div>

  </div>

</div>

<?php get_footer(); ?>
