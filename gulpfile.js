var config = require('./config/gulp-config.js');

var gulp       = require('gulp'),
autoprefixer = require('gulp-autoprefixer'),
minifycss    = require('gulp-uglifycss'),
filter       = require('gulp-filter'),
uglify       = require('gulp-uglify'),
imagemin     = require('gulp-imagemin'),
newer        = require('gulp-newer'),
rename       = require('gulp-rename'),
concat       = require('gulp-concat'),
notify       = require('gulp-notify'),
cmq          = require('gulp-combine-media-queries'),
runSequence  = require('gulp-run-sequence'),
sass         = require('gulp-sass'),
plugins      = require('gulp-load-plugins')({ camelize: true }),
ignore       = require('gulp-ignore'),
rimraf       = require('gulp-rimraf'),
zip          = require('gulp-zip'),
cache        = require('gulp-cache'),
sourcemaps   = require('gulp-sourcemaps'),
defaultTasks = [];

// var jsFiles =
// {
//   app:[
//     './assets/js/app/lib/ben/loader.js',
//     './assets/js/app/views/home.js',
//   ]
// };
//
// for (var key in jsFiles) {
//   createTask(key);
//   defaultTasks.push(key);
// }

gulp.task('sass', function(){
  return gulp.src('./assets/scss/style.scss')
    .pipe(sass())
    .pipe(autoprefixer('last 4 version', '> 1%', 'safari 5', 'ie 8', 'ie 9', 'opera 12.1', 'ios 6', 'android 4'))
    .on('error', consoleError)
    .pipe(gulp.dest('.'))
});

// gulp.task('jswatch', defaultTasks, function(){
//     for (var key in jsFiles)
//     { gulp.watch(jsFiles[key], [key]) }
// });

gulp.task('appJS', function() {
	return 	    gulp.src('./assets/js/app/**/*.js')
      .on('error', consoleError)
			.pipe(concat('app.js'))
			.pipe(gulp.dest('./assets/js'))
			.pipe(rename( {
				basename: "app",
				suffix: '.min'
			}))
			.pipe(uglify())
			.pipe(gulp.dest('./assets/js/'))
});

gulp.task('vendorsJS', function() {
	return 	    gulp.src(['./assets/js/vendor/**/*.js'])
      .on('error', consoleError)
			.pipe(concat('vendors.js'))
			.pipe(gulp.dest('./assets/js'))
			.pipe(rename( {
				basename: "vendors",
				suffix: '.min'
			}))
			.pipe(uglify())
			.pipe(gulp.dest('./assets/js/'))
});

function consoleError (error) {
  console.log(error.toString())
  this.emit('end')
}

gulp.task('watch', ['sass', 'vendorsJS', 'appJS'], function(){
  gulp.watch('./assets/js/vendor/*.js', ['vendorsJS']);
  gulp.watch('./assets/scss/**/*.scss', ['sass']);
  gulp.watch('./assets/js/vendor/*.js', ['vendorsJS']);
  // gulp.watch('./assets/js/app/*.js', ['appJS']);
});
