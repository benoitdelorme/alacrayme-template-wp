<?php

$urlJS = get_stylesheet_directory_uri() . '/assets/js';
$urlCSS = get_stylesheet_directory_uri() . '/style.css';

//load JS files
function addScripts() {
  global $urlJS;

  // wp_enqueue_script( 'TweenMax', "{$urlJS}/vendor/TweenMax.min.js", '', '', true);
  // wp_enqueue_script( 'jQuery', "{$urlJS}/vendor/jquery-3.3.1.min.js", '', '', true);

  // config
  wp_enqueue_script( 'Config', get_stylesheet_directory_uri()."/config/config.js", '', '', true);

  // vendors
  wp_enqueue_script( 'Vendors', "{$urlJS}/vendors.js", '', '', true);

  // core
  wp_enqueue_script( 'BackgroundSizeCover', "{$urlJS}/app/lib/ben/backgroundSizeCover.js", '', '', true);

  wp_enqueue_script( 'Loader', "{$urlJS}/app/lib/ben/loader.js", '', '', true);
  // wp_enqueue_script( 'App', "{$urlJS}/app.js", '', '', true);
  wp_enqueue_script( 'Home', "{$urlJS}/app/views/home.js", '', '', true);
  wp_enqueue_script( 'App', "{$urlJS}/app/app.js", '', '', true);
}

function removeBlackRibbon() {
  show_admin_bar(false);
}

function addStyles() {
  global $urlCSS;
	wp_enqueue_style( 'oxo-style', $urlCSS );
}

function addSupportTheme() {
  add_theme_support( 'menus' );
}

removeBlackRibbon();
addSupportTheme();

// Front
add_action( 'wp_enqueue_scripts', 'addStyles' );
add_action( 'wp_enqueue_scripts', 'addScripts' );

//Login
// add_action( 'login_enqueue_scripts', 'addStyles' );
// add_action( 'login_enqueue_scripts', 'addScripts' );

//Admin
// add_action( 'admin_enqueue_scripts', 'addStyles' );
// add_action( 'admin_enqueue_scripts', 'addScripts' );
