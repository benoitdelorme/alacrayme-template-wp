<?php
/**
 * @package OxO
 * @subpackage OxO Template
 * @since OxO Template v1
 */

get_header(); ?>

<div id="primary" class="content-area">
	<main id="main" class="site-main" role="main">
		<?php
		
		while ( have_posts() ) : the_post();

			get_template_part( 'views/content', 'page' );

		endwhile;
		?>

	</main>

</div>

<?php get_sidebar(); ?>
<?php get_footer(); ?>
