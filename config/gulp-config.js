exports.project = 'mip';
exports.url = 'mip.dev';

exports.build = './build/';
exports.buildInclude 	= [
  '**/*.php',
  '**/*.html',
  '**/*.css',
  '**/*.js',
  '**/*.svg',
  '**/*.ttf',
  '**/*.otf',
  '**/*.eot',
  '**/*.woff',
  '**/*.woff2',
  'screenshot.png',
  '!node_modules/**/*',
  '!assets/js/app/**/*',
  '!style.css.map'
];
