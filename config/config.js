var DEBUG = true;
var APPURL = "locahost:8888";
var APPDEBUGURL = "locahost:8888";


if(!DEBUG){
    if(!window.console) window.console = {};
    var methods = ["log", "debug", "warn", "info"];
    for(var i=0;i<methods.length;i++){
        console[methods[i]] = function(){};
    }
}
